#!/usr/bin/env bash

# we will switch between development and production, preserving a change each way.
# this depends on settings.php reading $DRUPAL_ENVIRONMENT to set $environment

echo "First we verify that setting the environment locally translates to setting config in Drupal."
echo "👉 drush php-eval + DRUSH_ENVIRONMENT"
DRUPAL_ENVIRONMENT=production drush php-eval '
  $config = \Drupal::configFactory();
  $entry = $config->get("config_split.config_split.production");
  \Drupal::logger("drush")
    ->notice("DRUPAL_ENVIRONMENT=@env, development=@dev, production=@prod", [
        "@env" => getenv("DRUPAL_ENVIRONMENT"),
        "@dev" => var_export($config->get("config_split.config_split.development")->get("status"), 1),
        "@prod" => var_export($config->get("config_split.config_split.production")->get("status"), 1),
       ]);
'
DRUPAL_ENVIRONMENT=development drush php-eval '
  $config = \Drupal::configFactory();
  $entry = $config->get("config_split.config_split.production");
  \Drupal::logger("drush")
    ->notice("DRUPAL_ENVIRONMENT=@env, development=@dev, production=@prod", [
        "@env" => getenv("DRUPAL_ENVIRONMENT"),
        "@dev" => var_export($config->get("config_split.config_split.development")->get("status"), 1),
        "@prod" => var_export($config->get("config_split.config_split.production")->get("status"), 1),
       ]);
'

echo
echo "We expect to see Syslog be active once we import config for production environment."
echo "👉 DRUPAL_ENVIRONMENT=production drush -y cim"
DRUPAL_ENVIRONMENT=production drush -y cim

echo
echo "Check module statuses:"
echo "👉 DRUPAL_ENVIRONMENT=production drush pm:list | grep -E '(devel|syslog)'"
DRUPAL_ENVIRONMENT=production drush pm:list | grep -E '(devel|syslog)'

echo
echo "We now make a change that will be preserved for production environment."
ENV_NAME=PROD-$( date +%s )
echo "👉 drush -y config-set system.site name $ENV_NAME"
DRUPAL_ENVIRONMENT=production drush -y config-set system.site name $ENV_NAME

echo
echo "Clear cache - uncertain if required"
echo "👉 DRUPAL_ENVIRONMENT=production drush -y cr"
DRUPAL_ENVIRONMENT=production drush -y cex

echo
echo "We now export our config, which should save it to production config."
echo "👉 DRUPAL_ENVIRONMENT=production drush -y cex"
DRUPAL_ENVIRONMENT=production drush -y cex

echo
echo "Let's see if the config got saved."
echo "👉 git diff"
git diff

echo
echo "Now we expect to see development modules enabled when we import config for development environment."
echo "👉 DRUPAL_ENVIRONMENT=development drush -y cim"
DRUPAL_ENVIRONMENT=development drush -y cim

echo
echo "Check module statuses:"
echo "👉 DRUPAL_ENVIRONMENT=production drush pm:list | grep -E '(devel|syslog)'"
DRUPAL_ENVIRONMENT=production drush pm:list | grep -E '(devel|syslog)'

echo
echo "We now make a change that should be preserved for development environment."
ENV_NAME=DEV-$( date +%s )
echo "👉 drush -y config-set system.site name $ENV_NAME"
DRUPAL_ENVIRONMENT=development drush -y config-set system.site name $ENV_NAME

echo
echo "Clear cache - uncertain if required"
echo "👉 DRUPAL_ENVIRONMENT=development drush -y cr"
DRUPAL_ENVIRONMENT=development drush -y cex

echo
echo "We now export our config, which should save it to development config."
echo "👉 DRUPAL_ENVIRONMENT=development drush -y cex"
DRUPAL_ENVIRONMENT=development drush -y cex

echo
echo "Let's see if the config got saved."
echo "👉 git diff"
git diff

echo
