Vagrant.configure('2') do |config|
  # Since they may be used for naming databases, hyphens are not recommended for
  # use in project_name.
  project_name = 'freshdrupal'

  config.catalyst.platform = 'ubuntu-18.04'

  # Change to this directory on "vagrant ssh".
  config.catalyst.working_directory = '/vagrant'

  config.vm.hostname = project_name + '.local'

  config.hostmanager.enabled = true
  config.hostmanager.manage_host = true
  config.hostmanager.manage_guest = false
  config.hostmanager.ignore_private_ip = false

  config.vm.provision :catalyst do |cat|
    cat.project_type = 'drupal8'
    cat.project_name = project_name

    # For more setting options, see:
    # http://devtools-docs.wgtn.cat-it.co.nz/catalyst-vagrant/catalyst-provisioner.html
    cat.hiera_data = {
      'devenv::project::drupal::vendor_scripts' => ['drupal', 'drush'],
    }

    # Override Drupal environment name:
    # cat.hiera_data['devenv::project::drupal::environment'] = 'test',

    # Override with explicit NodeJS version:
    # cat.hiera_data['devenv::tools::nodejs::version'] = '8.x'

    # Replace the default components
    cat.services = [
      'mysql'
    ]

    # Add local memcache daemon
    # cat.services << 'memcached'

    # Tools
    cat.tools = [
      'xdebug',
      'composer',
      'git',
      'mailhog',
    ]

    # Add mailhog for email testing
    # cat.tools << 'mailhog'
  end
end
